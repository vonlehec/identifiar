//
//  VisionHelper.swift
//  IdentifiAR
//
//  Created by Charles von Lehe on 10/2/17.
//  Copyright © 2017 Charles von Lehe. All rights reserved.
//

import Foundation
import Vision

//VisionHelper - Handles all requests to Inceptionv3 model
class VisionHelper {
    private let inceptionModel = Inceptionv3()

    func performVisionRequest (pixelBuffer:CVPixelBuffer, completion:@escaping (_ identifier:String)->Void) {
        
        //Create image request
        let request = getMLRequest(completion: completion)
        
        //Create image request handler from passed in image
        let imageRequestHandler = VNImageRequestHandler(cvPixelBuffer: pixelBuffer, orientation: .upMirrored, options: [:])
        DispatchQueue.global().async {
            do {
                //Perform the image request
                try imageRequestHandler.perform([request])
            }catch {
                fatalError("Failed to perform request")
            }
        }
        
    }
    
    private func getMLRequest (completion:@escaping (_ identifier:String)->Void) -> VNCoreMLRequest {
        //Create vision model from inception model
        guard let visionModel = try? VNCoreMLModel(for: inceptionModel.model) else { fatalError("Could not load model") }
        let request = VNCoreMLRequest(model: visionModel) {request, error in
            guard error == nil else {
                print("Error getting result: \(error.debugDescription)")
                return
            }
            //Fetch observation from request that has the highest confidence
            guard let observation = request.results?.first as? VNClassificationObservation else {return}
            DispatchQueue.main.async {
                
                //Sending observation back to completion
                completion(observation.identifier)
            }
        }
        request.imageCropAndScaleOption = .centerCrop
        
        return request
    }
}
