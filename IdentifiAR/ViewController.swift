//
//  ViewController.swift
//  IdentifiAR
//
//  Created by Charles von Lehe on 10/2/17.
//  Copyright © 2017 Charles von Lehe. All rights reserved.
//

import UIKit
import SceneKit
import ARKit

class ViewController: UIViewController, ARSCNViewDelegate {
    @IBOutlet weak var overlayView: UIView!
    @IBOutlet weak var okayButton: UIButton!
    
    @IBOutlet var sceneView: ARSCNView!
    
    //Initialize vision helper
    let visionHelper = VisionHelper()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Set the view's delegate
        sceneView.delegate = self
        okayButton.layer.cornerRadius = 4.0
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        
        guard
            let currentFrame = sceneView.session.currentFrame?.capturedImage, //Get image from sceneView
            let touchLoacation = touches.first?.location(in: sceneView), //Get location of the user's touch
            let result = sceneView.hitTest(touchLoacation, types: .featurePoint).first else {return} //Get 3D position of user's touch from 2D location
        
        
        visionHelper.performVisionRequest(pixelBuffer: currentFrame) { predictionName in
            self.displayPredictions(predictionName: predictionName, hitTestResult: result)
        }
    }
    

    

    
    func displayPredictions (predictionName: String, hitTestResult:ARHitTestResult) {
        
        //Add sphere to location the user tapped
        sceneView.scene.rootNode.addChildNode(createSphereNode(hitTestResult: hitTestResult))
        
        //Add label to location the user tapped with text from object prediction
        sceneView.scene.rootNode.addChildNode(createTextNode(text: predictionName, hitTestResult: hitTestResult))
    }
    
    private func createSphereNode (hitTestResult:ARHitTestResult) -> SCNNode {
        //Create sphere geometry
        let sphere = SCNSphere(radius: 0.01)
        
        //Set sphere color
        let material = SCNMaterial()
        material.diffuse.contents = #colorLiteral(red: 0.1880986989, green: 0.8226090074, blue: 0.5176928043, alpha: 1)
        sphere.firstMaterial = material
        
        //Create sphere node from sphere geometry
        let sphereNode = SCNNode(geometry: sphere)
        
        //Set 3D postion of sphere node
        sphereNode.position = SCNVector3(hitTestResult.worldTransform.columns.3.x, hitTestResult.worldTransform.columns.3.y, hitTestResult.worldTransform.columns.3.z)

        return sphereNode
    }
    
    private func createTextNode (text:String, hitTestResult:ARHitTestResult) -> SCNNode {
        //Create text geometry
        let textGeometry = SCNText(string: text, extrusionDepth: 0)
        textGeometry.alignmentMode = kCAAlignmentCenter
        
        //Set text color and font
        textGeometry.firstMaterial?.diffuse.contents = #colorLiteral(red: 0.2588235438, green: 0.7568627596, blue: 0.9686274529, alpha: 1)
        textGeometry.firstMaterial?.specular.contents = UIColor.white
        textGeometry.font = UIFont(name: "Futura", size: 0.15)
        
        //Create text node from text geometry
        let textNode = SCNNode(geometry: textGeometry)
        
        //Set node size
        textNode.scale = SCNVector3(0.05, 0.05, 0.05)
        
        //Set node position
        textNode.position = SCNVector3(hitTestResult.worldTransform.columns.3.x - 0.01, hitTestResult.worldTransform.columns.3.y - 0.01, hitTestResult.worldTransform.columns.3.z)

        return textNode
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // Create a session configuration
        let configuration = ARWorldTrackingConfiguration()

        // Run the view's session
        sceneView.session.run(configuration)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        // Pause the view's session
        sceneView.session.pause()
    }
    
    @IBAction func okayButtonPressed(_ sender: UIButton) {
        overlayView.isHidden = true
    }
    
}
